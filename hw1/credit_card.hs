-- converts an integer to a list of its digits
-- only works for positive integers
toDigits :: Integer -> [Integer]
toDigits n
  | n > 9     = (toDigits (quot n 10)) ++ [(mod n 10)]
  | otherwise = [n]

-- doubles every second number in the list (starting from the right hand side)
doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther list = reverse (doubleEveryOtherHelper (reverse list) 0)

-- helper method for the recursive doubleEveryOther call
doubleEveryOtherHelper :: [Integer] -> Integer -> [Integer]
doubleEveryOtherHelper [] n = []
doubleEveryOtherHelper list n
  | (mod n 2) == 1     = (2 * (head list)) : doubleEveryOtherHelper (drop 1 list) (n+1)
  | otherwise          = (head list) : doubleEveryOtherHelper (drop 1 list) (n+1)

-- sums all the digits in a list of integers
sumDigitsOfList :: [Integer] -> Integer
sumDigitsOfList [] = 0
sumDigitsOfList list = (sum (toDigits(head list))) + (sumDigitsOfList (drop 1 list))

-- validates the integer is a credit card number
validate :: Integer -> Bool
validate n = (mod (sumDigitsOfList (doubleEveryOther (toDigits n))) 10) == 0